x = "Hello world"
i = 0
while i < len(x):
    while x[i] == "l":
        print("e", end="")
        i += 1
    while x[i] == "o":
        print("a", end="")
        i += 1
    print(x[i], end="")
    i += 1
print()

x = "Hello world"
for i in range(len(x)):
    if x[i] == "o":
        print("a", end="")
    elif x[i] == "l":
        print("e", end="")
    else:
        print(x[i], end="")
