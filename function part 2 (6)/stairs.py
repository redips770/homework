def stairs(n):
    if n == 1 or n == 0:
        return 1
    else:
        return stairs(n - 1) + stairs(n - 2)


x = int(input("Введите номер ступеньки:"))
print(f"На ступеньку номер {x} можно подняться {stairs(x)} способами")
