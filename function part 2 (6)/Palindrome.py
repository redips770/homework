def palindrome(word):
    n = str(word.lower().replace(" ", "").replace(",", "").replace(".",""))
    if n == n[::-1]:
        print("Является палиндромом")
    else:
        print("Не является палиндромом")


palindrome("А роза упала на лапу Азора")
palindrome("Ешь немытого ты меньше")
palindrome("Умер, и мир ему.")
palindrome("Обычная фраза, не являющаяся палиндромом.")