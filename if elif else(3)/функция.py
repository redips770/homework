import math

x = int(input("Введите число:"))
if x < -math.pi or x > math.pi:
    y = x
else:
    y = math.cos(3 * x)
print(y)
