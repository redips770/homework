my_list = [1, 2, 3, 4, 5]

print(f"Наибольший элемент списка: {max(my_list)}", end="\n")
print(f"Наименьший элемент списка: {min(my_list)}", end="\n")
print(f"Сумма всех элементов списка равна: {sum(my_list)}", end="\n")
print(f"Среднее арифмитическое значений списка равно: {sum(my_list)/len(my_list)}")
