def stairs(n):
    m = []
    z = 1
    for i in range(n):
        m.append(z)
        z += m[i-1]
    return max(m)


x = int(input("Введите номер ступеньки:"))
print(f"На ступеньку номер {x} можно подняться {stairs(x)} способами")
