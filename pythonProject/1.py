x = "Hello, World!"
n = 0
while n < len(x):
    if not n % 2:
        print("_", end="")
        n += 1
    elif n % 2 and x[n]=="l":
        print("L", end="")
        n+=1
    else:
        print(x[n], end="")
        n+=1
