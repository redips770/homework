STRING = "Hello, World!"
for i in range(0, len(STRING)):
    if not i % 2:
        print("_", end="")
    elif STRING[i] == "l":
        print("L", end="")
    else:
        print(STRING[i], end="")
