import math


def max_del(x, y):
    return (x / math.gcd(x, y), y / math.gcd(x, y))


x = int(input("Введите первое число:"))
y = int(input("Введите второе число:"))
print(max_del(x, y))
