
def add(x, y):
    return x + y


def sub(x, y):
    return x - y


def mul(x, y):
    return x * y


def div(x, y):
    if not y:
        return ("Деление на ноль!")
    else:
        return x / y


while True:
    print("1. Сложение")
    print("2. Вычитание")
    print("3. Умножение")
    print("4. Деление")
    print("5. Выход")
    response = input("Выберите действие:")
    print()
    if response == "1":
        x = int(input("Введите первое число:"))
        y = int(input("Введите второе число:"))
        print(add(x, y))
    elif response == "2":
        x = int(input("Введите первое число:"))
        y = int(input("Введите второе число:"))
        print(sub(x, y))
    elif response == "3":
        x = int(input("Введите первое число:"))
        y = int(input("Введите второе число:"))
        print(mul(x, y))
    elif response == "4":
        x = int(input("Введите первое число:"))
        y = int(input("Введите второе число:"))
        print(div(x, y))
    elif response == "5":
        break
    else:
        print("Некорректный ввод")
    print()
